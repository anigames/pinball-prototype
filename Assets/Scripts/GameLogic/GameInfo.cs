﻿using UnityEngine;
using System.Collections;

public class GameInfo : MonoBehaviour
{
    static GameInfo instance = null;
    
#pragma warning disable

    [SerializeField]
    private bool isLogEnable = true;

    [Header("Game Play")]
    [Space(10)]    
    [SerializeField]
    private int total_tries = 3;
    [SerializeField]
    private float max_velocity_magnitude = 3;

    [Space(10)]
    [Header("Control")]     
    [SerializeField]
    [Range(0.01f,5f)]
    private float forcing_speed = 1;

    [Header("Automode")]
    [Space(10)]    
    [SerializeField]
    float auto_force_delay = 2;

#pragma warning restore

    public static bool IsLogEnable { get { return GameInfo.instance.isLogEnable; } }
    public static int TotalTries { get { return GameInfo.instance.total_tries; } }
    public static float MaxVelocity { get { return GameInfo.instance.max_velocity_magnitude; } }
    public static float ForcingSpeed { get { return GameInfo.instance.forcing_speed; } }

    public static float AutoForceDelay { get { return GameInfo.instance.auto_force_delay; } }

    public static bool IsUserControl { get; set; }

    void Awake ()
    {
        if (instance == null)
        {
            instance = this;
            IsUserControl = true;
        }
        else
        {
            if (instance != this)
                DestroyImmediate(this.gameObject);
        }
	}
	
}
