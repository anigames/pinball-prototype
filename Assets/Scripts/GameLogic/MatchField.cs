﻿using UnityEngine;
using System.Collections;

public class MatchField : MonoBehaviour
{


#pragma warning disable
    [SerializeField]
    Transform flipperLeft, flipperRight;

    [SerializeField]
    GameObject ball;

    [SerializeField]
    Vector3 startPos;

    Coroutine leftRoutine, rightRoutine;

    GameObject ballInstance = null;

#pragma warning restore

    void OnEnable ()
    {
        if (GameInfo.IsUserControl)
        {
            InputBase.OnLeftFlipperPush += LeftFlipperMove;
            InputBase.OnRightFlipperPush += RightFlipperMove;
            InputBase.OnKickBall += InputBase_OnKickBall;
        }

        
	}

    void OnDisable ()
    {
        if (GameInfo.IsUserControl)
        {
            InputBase.OnLeftFlipperPush -= LeftFlipperMove;
            InputBase.OnRightFlipperPush -= RightFlipperMove;
            InputBase.OnKickBall -= InputBase_OnKickBall;
        }

        #region reset flippers
        flipperLeft.localRotation = flipperRight.localRotation = Quaternion.Euler(Vector3.forward * -30);
        #endregion
    }

    public void EndRound()
    {
        if (ballInstance != null)
        {
            Destroy(ballInstance);
            ballInstance = null;
            System.GC.Collect();
        }
        GameController.Instance.State = GameState.EndRound;
    }

    public void InputBase_OnKickBall(float val)
    {
        ballInstance = Instantiate(ball) as GameObject;
        ballInstance.transform.parent = this.transform;
        ballInstance.transform.localScale = Vector3.one;
        ballInstance.transform.localPosition = startPos;
        ballInstance.GetComponent<Rigidbody2D>().velocity = (Vector2.up * -1 * val*10);
    }

    void LeftFlipperMove()
    {
        if(leftRoutine!=null)
            StopCoroutine(leftRoutine);
        leftRoutine = StartCoroutine(MoveFlipper(flipperLeft));
    }

    void RightFlipperMove()
    {
        if(rightRoutine!=null)
            StopCoroutine(rightRoutine);
        rightRoutine = StartCoroutine(MoveFlipper(flipperRight));
    }

    public void AILeftFlipperMove()
    {
        if (!GameInfo.IsUserControl)
            LeftFlipperMove();
    }

    public void AIRightFlipperMove()
    {
        if (!GameInfo.IsUserControl)
            RightFlipperMove();
    }

    IEnumerator MoveFlipper(Transform flipper)
    {

        while ( WrapAngle(flipper.eulerAngles.z) < 30)
        {
            flipper.Rotate(Vector3.forward,Time.deltaTime*200);
            yield return new WaitForSeconds(0.01f);
        }

        while (WrapAngle(flipper.eulerAngles.z) > -30)
        {
            flipper.Rotate(Vector3.forward, Time.deltaTime * -200);
            yield return new WaitForSeconds(0.01f);
        }
    }


    float WrapAngle(float angle)
    {
        if (angle > 180)
        {
            angle -= 360;
        }
        else
        {
            if (angle < -180)
                angle += 360;
        }
        return angle;
    }
}
