﻿using UnityEngine;
using System.Collections;

public class Session
{

    GameScore currentScore;
    GameScore totalScore;

    int tries;

    public int Tries { get { return tries; } }
    public GameScore CurrentScore { get { return currentScore; } }
    public GameScore TotalScore { get { return totalScore; } }

    public event System.Action<int> OnCollectPoints;

    public Session()
    {
        currentScore = new GameScore();
        totalScore = new GameScore();
        tries = 0;
    }

    public void StartRound()
    {
        currentScore = new GameScore();
    }

    public void EndRound()
    {
        totalScore += currentScore;
        tries++;
    }

    public void EndSession()
    {
        currentScore = null;
        totalScore = null;
    }


    public void CollectPoints(int pts)
    {
        currentScore.AddPoints(pts);
        if(OnCollectPoints!=null)
            OnCollectPoints(pts);
    }

}
