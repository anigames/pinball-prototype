﻿using UnityEngine;
using System.Collections;

public class GameScore
{

    public int Points { get; private set; }

    public GameScore()
    {
        Points = 0;
    }

    public void AddPoints(int points)
    {
        Points += points;
    }

    public static GameScore operator +(GameScore a, GameScore b)
    {

        GameScore total = new GameScore();
        total.Points = a.Points + b.Points;

        return total;
    }
    	
}
