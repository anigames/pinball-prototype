﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{

    static GameController _instance = null;
    public static GameController Instance { get { return _instance; } }

    /*
    public static event System.Action OnMenuOpen = delegate { if (GameInfo.IsLogEnable) Debug.Log("Menu started!"); };
    public static event System.Action OnSessionStart = delegate { if (GameInfo.IsLogEnable) Debug.Log("Session started!"); };
    public static event System.Action OnRoundStart = delegate { if (GameInfo.IsLogEnable) Debug.Log("Round started!"); };
    public static event System.Action OnForcing = delegate { if (GameInfo.IsLogEnable) Debug.Log("Forcing..."); };
    public static event System.Action OnPlayingStart = delegate { if (GameInfo.IsLogEnable) Debug.Log("Playing..."); };
    public static event System.Action OnRoundEnd = delegate { if (GameInfo.IsLogEnable) Debug.Log("Round Over!"); };
    public static event System.Action OnGameEnd = delegate { if (GameInfo.IsLogEnable) Debug.Log("Game Over!"); };*/

    public static event System.Action<GameState> OnStateChange = t => { if (GameInfo.IsLogEnable) Debug.Log("State: " + t); };

    public Session GameSession = null;

    [SerializeField]
    public MatchField MatchField;

	void Start ()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
            State = GameState.Menu;
            MatchField.gameObject.SetActive(false);
        }
        else
        {
            if (_instance != this)
                DestroyImmediate(gameObject);
        }
	}


    private GameState _state = GameState.None;
    public GameState State
    {
        get { return _state; }
        set
        {
            if (_state == value)
                return;
            _state = value;

            UIController.Instance.SetUI(value);

            

            switch (value)
            {
                case GameState.None:                    
                    break;
                case GameState.Menu:
                    break;
                case GameState.NewSession:

                    GameSession = new Session();                    
                    break;

                case GameState.RoundDeclaration:
                    GameSession.StartRound();
                    MatchField.gameObject.SetActive(true);
                    break;

                case GameState.Forcing:
                    break;
                case GameState.Playing:
                    break;
                case GameState.EndRound:
                    MatchField.gameObject.SetActive(false);
                    GameSession.EndRound();
                    break;
                case GameState.Result:
                    GameSession.EndSession();
                    break;
                default: break;

            }

            OnStateChange(value);

        }
    }

}
