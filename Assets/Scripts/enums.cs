﻿public enum GameState { None, Menu, NewSession, RoundDeclaration, Forcing, Playing, EndRound, Result }
public enum ControlPlatform { Keyboard, Touches, Gamepad, VR }