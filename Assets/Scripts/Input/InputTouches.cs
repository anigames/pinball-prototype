﻿using UnityEngine;
using System.Collections;

public class InputTouches : InputBase
{

    bool isForcing = false;

#pragma warning disable

    [SerializeField]
    GameObject btnKick, btnLeft, btnRight;

#pragma warning restore

    void Start()
    {
        GameController.OnStateChange += OnStateChange;
    }

    void OnDestroy()
    {
        GameController.OnStateChange -= OnStateChange;
    }

    void OnStateChange(GameState state)
    {
        switch (state)
        {
            case GameState.Forcing:
                btnKick.SetActive(true);
                btnLeft.SetActive(false);
                btnRight.SetActive(false);
                break;
            case GameState.Playing:
                btnKick.SetActive(false);
                btnLeft.SetActive(true);
                btnRight.SetActive(true);
                break;
            default:
                btnKick.SetActive(false);
                btnLeft.SetActive(false);
                btnRight.SetActive(false);
                break;
        }
    }

    void Update()
    {

        if (GameController.Instance.State == GameState.Forcing && isForcing)
        {

            val += (target - val > 0 ? 1 : -1) * GameInfo.ForcingSpeed * Time.deltaTime;
            
            if (Mathf.Abs(val - target) < 0.01f)
            {
                target = ChangeTarget(target);
            }
            
            base.ChangeProgress(val);

        }

    }

    public void ForcingStart()
    {
        isForcing = true;
        val = 0;
        target = 1;
    }
    public void ForcingStop()
    {
        isForcing = false;
        KickBall(val);
    }

    public void TouchLeft()
    { base.PushLeftFlipper(); }
    public void TouchRight()
    { base.PushRightFlipper(); }

}
