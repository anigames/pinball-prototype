﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
#pragma warning disable 

    [SerializeField]
    private InputBase[] inputs;

    InputBase cur_input;
#pragma warning restore 

    void Start ()
    {

        bool isInputFound = false;

        ControlPlatform cPlatform = ControlPlatform.Keyboard;

#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID)
        cPlatform = ControlPlatform.Touches;
#endif


        foreach (InputBase ib in inputs)
        {
            if (ib.controlPlatform == cPlatform)
            {
                ib.gameObject.SetActive(true);
                isInputFound = true;
                if (GameInfo.IsLogEnable)
                    Debug.Log(ib.controlPlatform.ToString() + " input is enabled");
            }
            else
                ib.gameObject.SetActive(false);
        }


        if (!isInputFound)
        {
            Debug.LogError(Application.platform + " platform is not supported!");
            Debug.Break();
        }


    }
	
	
	void Update () {
	
	}
}
