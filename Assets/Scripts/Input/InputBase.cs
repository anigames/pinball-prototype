﻿using UnityEngine;
using System.Collections;

public class InputBase : MonoBehaviour
{

    public static event System.Action<float> OnKickBall;
    public static event System.Action<float> OnChangeProgress;
    public static event System.Action OnLeftFlipperPush;
    public static event System.Action OnRightFlipperPush;

    public ControlPlatform controlPlatform;


    protected float val = 0;
    protected float target = 1;

    void OnStart()
    {
        GameController.OnStateChange += t => { if (t == GameState.RoundDeclaration) Reset(); };
    }

    void Reset()
    {
        val = 0;
        target = 1;
    }


    protected void KickBall(float val)
    {
        if(OnKickBall!=null)
            OnKickBall(val);
        GameController.Instance.State = GameState.Playing;
    }

    protected void ChangeProgress(float val)
    {
        if (OnChangeProgress!=null)
            OnChangeProgress(val);
    }

    protected void PushLeftFlipper()
    {
        if (OnLeftFlipperPush!=null)
            OnLeftFlipperPush();
    }

    protected void PushRightFlipper()
    {
        if (OnRightFlipperPush!=null)
            OnRightFlipperPush();
    }

    protected float ChangeTarget(float t)
    {

        if (t == 0)
            return 1;
        else
            return 0;
    }
}
