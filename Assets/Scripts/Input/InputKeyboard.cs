﻿using UnityEngine;
using System.Collections;

public class InputKeyboard : InputBase
{

    void Update()
    {

        if (GameController.Instance.State == GameState.Playing)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                base.PushLeftFlipper();

            if (Input.GetKeyDown(KeyCode.RightArrow))
                base.PushRightFlipper();
        }

        if (GameController.Instance.State == GameState.Forcing)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                val += (target - val > 0 ? 1 : -1) * GameInfo.ForcingSpeed * Time.deltaTime;

                if (Mathf.Abs(val - target) < 0.01f)
                {
                    target = ChangeTarget(target);
                }

                base.ChangeProgress(val);

            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                KickBall(val);
            }
        }

    }

    
}
