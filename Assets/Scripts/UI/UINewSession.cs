﻿using UnityEngine;
using System.Collections;

public class UINewSession : UIElementBase
{

    [SerializeField]
    [Header("Delay before new State")]
    float delay = 2;
	
	void OnEnable ()
    {
        Invoke("NextState", delay);
	}
	
	
	void NextState ()
    {
        GameController.Instance.State = GameState.RoundDeclaration;
	}
}
