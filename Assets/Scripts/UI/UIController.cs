﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour
{
    public static UIController Instance { get; private set; }

#pragma warning disable

    [SerializeField]
    UIElementBase[] _elements;


#pragma warning restore 

    void Awake ()
    {
        Instance = this;

	}
	
	
	public void SetUI (GameState state)
    {
        foreach (UIElementBase e in _elements)
        {
            if (e.State == state)
                e.Open();
            else
                e.Close();
        }

    }



}
