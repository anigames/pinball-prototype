﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIResult : UIElementBase
{


#pragma warning disable

    [SerializeField]
    Text txtPointsValue;

#pragma warning restore

    void OnEnable ()
    {
        txtPointsValue.text = GameController.Instance.GameSession.TotalScore.Points.ToString();
	}

	void OnDisable ()
    {
	
	}

    public void EndSession()
    {
        GameController.Instance.State = GameState.Menu;
    }
}
