﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIForcing : UIElementBase
{

    [SerializeField]
    Image imgProgress;



	void OnEnable ()
    {
        if (GameInfo.IsUserControl)
        {
            InputBase.OnChangeProgress += InputBase_OnChangeProgress;
        }
        else
        {
            StartCoroutine(AutoForcing());
        }
	}

    void OnDisable ()
    {
        if (GameInfo.IsUserControl)
        {
            InputBase.OnChangeProgress -= InputBase_OnChangeProgress;
        }
    }

    private void InputBase_OnChangeProgress(float obj)
    {
        imgProgress.fillAmount = obj;
    }

    IEnumerator AutoForcing()
    {

        yield return new WaitForSeconds(GameInfo.AutoForceDelay);

        float force = Random.Range(0.1f, 1f);
        float cur_force = 0;
        while (cur_force < force)
        {
            cur_force = Mathf.Clamp(cur_force + GameInfo.ForcingSpeed*Time.deltaTime,0,force);
            imgProgress.fillAmount = cur_force;
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
        GameController.Instance.MatchField.InputBase_OnKickBall(force);
    }
}
