﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

public class ButtonEvents : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    UnityEvent OnDown;
    [SerializeField]
    UnityEvent OnUp;

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDown.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnUp.Invoke();
    }
}
