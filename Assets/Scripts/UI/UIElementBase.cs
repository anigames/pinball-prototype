﻿using UnityEngine;
using System.Collections;

public class UIElementBase : MonoBehaviour
{

    [SerializeField]
    GameState state;

    public GameState State { get { return state; } }

    public void Open()
    {
        Debug.Log("Open " + state.ToString());
        this.gameObject.SetActive(true);
    }

    public void Close()
    {
        this.gameObject.SetActive(false);
    }

}
