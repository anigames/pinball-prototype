﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIRoundDeclaration : UIElementBase
{

    [SerializeField]
    Text txtTitle;

    [SerializeField]
    [Header("Delay before new State")]
    float delay = 1;

    void OnEnable ()
    {
        txtTitle.text = "Round " + (GameController.Instance.GameSession.Tries + 1).ToString() + "/" + GameInfo.TotalTries;
        Invoke("NextState", delay);
    }


    void NextState()
    {
        GameController.Instance.State = GameState.Forcing;
    }
}
