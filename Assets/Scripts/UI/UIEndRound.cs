﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIEndRound : UIElementBase
{

#pragma warning disable

    [SerializeField]
    Text txtPointsValue;

#pragma warning restore

    void OnEnable ()
    {
        txtPointsValue.text = GameController.Instance.GameSession.CurrentScore.Points.ToString();
	}
	
	
	void OnDisable ()
    {
	
	}

    public void Continue()
    {
        if (GameController.Instance.GameSession.Tries < GameInfo.TotalTries)
        {
            GameController.Instance.State = GameState.RoundDeclaration;
        }
        else
        {
            GameController.Instance.State = GameState.Result;
        }
    }
}
