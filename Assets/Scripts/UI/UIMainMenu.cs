﻿using UnityEngine;
using System.Collections;

public class UIMainMenu : UIElementBase
{


    public void StartGame(bool isUserControl)
    {
        GameInfo.IsUserControl = isUserControl;
        GameController.Instance.State = GameState.NewSession;
    }


}
