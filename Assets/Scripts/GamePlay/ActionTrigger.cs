﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class ActionTrigger : MonoBehaviour
{
#pragma warning disable
    [SerializeField]
    UnityEvent ev;
#pragma warning restore

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "ball")
            ev.Invoke();
    }
}
