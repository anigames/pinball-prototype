﻿using UnityEngine;
using System.Collections;

public class CollisionEffect : MonoBehaviour
{

#pragma warning disable

    [SerializeField]
    int Points;

#pragma warning restore

    void Start () {
	
	}
	
	
	void Update ()
    {
	    
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.tag == "ball")
            GameController.Instance.GameSession.CollectPoints(Points);
    }
}
