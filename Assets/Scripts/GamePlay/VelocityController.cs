﻿using UnityEngine;
using System.Collections;

public class VelocityController : MonoBehaviour
{
    Rigidbody2D rigid;

    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }
    /*
    IEnumerator PostUpdateCorrection()
    {
        yield return new WaitForSeconds(Time.fixedDeltaTime)
    }*/


    void OnCollisionExit2D(Collision2D col)
    {
        //StartCoroutine(PostUpdateCorrection());

        if (rigid.velocity.magnitude > GameInfo.MaxVelocity)
            rigid.velocity = rigid.velocity.normalized * GameInfo.MaxVelocity;

    }

}
